//
//  JobDetailViewController.swift
//  HaulioInterviewTest
//
//  Created by twizzyindy on 8/5/20.
//  Copyright © 2020 twizzyindy. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class JobDetailViewController: UIViewController {
    
    var locationManager: CLLocationManager!
    var currentLocation: CLLocation?
    var placesClient : GMSPlacesClient!
     var mapView: GMSMapView!
    
    var m_current_job = JobStruct(id: 0, job_id: 0, priority: 0, company: "", address: "", geolocation: GeolocationType(latitude: 0, longitude: 0))
    
    // search bar
    var resultsViewController : GMSAutocompleteResultsViewController?
    var searchController : UISearchController?
    var resultView: UITextView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // get current location
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        // Do any additional setup after loading the view.
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: m_current_job.geolocation.latitude, longitude: m_current_job.geolocation.longitude, zoom: 15.0)
        mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        self.view.addSubview(mapView)

        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: m_current_job.geolocation.latitude, longitude: m_current_job.geolocation.longitude)
        marker.title = m_current_job.company
        marker.snippet = m_current_job.address
        marker.map = mapView
        
        // set my location button to be appear
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        self.title = "JOB ID-\(m_current_job.job_id)"
        
        // add UISearchBar to mapview
        self.addToSubview()
    }
    
    //MARK: subview
    
    func addToSubview(){
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self

        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        
        let subView = UIView(frame: CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.origin.y)! + (self.navigationController?.navigationBar.frame.height)!, width: 350.0, height: 45.0))

        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false

        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}

//MARK: Extensions
//MARK: CLLocationManagerDelegate events
// Handle incoming location events.
extension JobDetailViewController :CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")

        //let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
        //                                longitude: location.coordinate.longitude,
        //                                zoom: 15.0)

        if mapView.isHidden {
            mapView.isHidden = false
            //mapView.camera = camera
        } else {
            //mapView.animate(to: camera)
        }

    }

    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        @unknown default:
            fatalError()
        }
    }

    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}

// Handle the user's selection.
  extension JobDetailViewController: GMSAutocompleteResultsViewControllerDelegate {
      func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                             didAutocompleteWith place: GMSPlace) {
          searchController?.isActive = false
          // Do something with the selected place.
          print("Place name: \(place.name)")
          print("Place address: \(String(describing: place.formattedAddress))")
          print("Place attributions: \(place.attributions)")
      }

      func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                             didFailAutocompleteWithError error: Error){
          // TODO: handle the error.
          print("Error: ", error.localizedDescription)
        
        //MARK: https://developers.google.com/places/ios-sdk/client-migration
//         To use the Places SDK for iOS, you must:
//        Include an API key with all API requests.
//        Enable billing on each of your projects.
//        Enable the Places API service for each of your projects.
        
        let dlgMessage = UIAlertController(title: "Confirm", message: "To use the Places SDK for iOS, you must enable Billing on each of your projects.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: {(action) -> Void in
            print("ok")
        })
        
        dlgMessage.addAction(ok)
        self.present(dlgMessage, animated: true, completion: nil)
        
      }

      // Turn the network activity indicator on and off again.
      func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
          UIApplication.shared.isNetworkActivityIndicatorVisible = true
      }

      func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
          UIApplication.shared.isNetworkActivityIndicatorVisible = false
      }
  }
