//
//  JobsListViewController.swift
//  HaulioInterviewTest
//
//  Created by twizzyindy on 8/4/20.
//  Copyright © 2020 twizzyindy. All rights reserved.
//

import Foundation
import UIKit
import GoogleSignIn

class JobsListViewController : UITableViewController
{
    var jobs = [JobStruct]()
    
        var m_user_model = UserModel(userId: "", userIdToken: "", user_first_name: "", user_last_name: "", user_email: "", user_profile_url: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navController = self.navigationController as? MainNavigationController
        self.m_user_model = navController!.m_user_model
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(imageLiteralResourceName: "Turn off"), style: .done, target: self, action: #selector(signOutBarButtonItemTapped))
        self.navigationItem.rightBarButtonItem?.tintColor = .white
        
        //add user info button
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(imageLiteralResourceName: "User-1"), style: .done, target: self,
                                                                action: #selector(userInfoButtonTapped))
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        
        //MARK: load json from web
        
        getJobsList()
        
    }
    
    func setUserModel(model: UserModel)
    {
        self.m_user_model = model
    }
    
    func getUserModel() -> UserModel
    {
        return self.m_user_model
    }
    
    //MARK: User Info button tapped
    @objc func userInfoButtonTapped()
    {
        
        let vc : UserInfoViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "id_userinfo_vc") as! UserInfoViewController
        vc.modalPresentationStyle = .popover
        
        vc.m_user_model = self.m_user_model
        
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: get data from json
    private func getJobsList()
    {
        let url = URL(string: "https://www.haulio.io/joblist.json")
        
        URLSession.shared.dataTask(with: url!) { (data,response,error) in
            
            if  error == nil {
                
                do {
                    self.jobs = try JSONDecoder().decode([JobStruct].self, from: data!)
                    //print(self.jobs)
                } catch {
                    print("JSON error")
                }
            }
        }.resume()
    }
    
    //TODO: Sign out current google account
    @objc func signOutBarButtonItemTapped()
    {
        GIDSignIn.sharedInstance()?.signOut()
        self.dismiss(animated: false, completion: nil)
    }
    
    //MARK: TableView data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jobs.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "jobslistcell", for: indexPath) as! JobItemTableViewCell
        
        //TODO: configure cell with decoded data
        cell.lblTitle?.text = String( "Job Number: \(self.jobs[indexPath.row].job_id)"  )
        
        cell.lblSubtitle?.text = "Company : \(self.jobs[indexPath.row].company)\nAddress : \(self.jobs[indexPath.row].address)";
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "jobdetailview", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "jobdetailview" {
            if let destination = segue.destination as? JobDetailViewController {
                destination.m_current_job = self.jobs[(tableView.indexPathForSelectedRow?.row)!]
            
            }
        }
    }
}
