//
//  ViewController.swift
//  HaulioInterviewTest
//
//  Created by twizzyindy on 8/4/20.
//  Copyright © 2020 twizzyindy. All rights reserved.
//

import UIKit
import GoogleSignIn


class LoginViewController: UIViewController {
    
    var m_google_signIn = GIDSignIn.sharedInstance()
    
    // got properties from sign in
    var m_user_model = UserModel(userId: "", userIdToken: "", user_first_name: "", user_last_name: "", user_email: "", user_profile_url: "")

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnSignInWithGoogle_TouchUpInside(_ sender: UIButton) {
        
        self.googleAuthLogin()
        
    }
    
    func googleAuthLogin()
    {
        self.m_google_signIn?.presentingViewController = self
        self.m_google_signIn?.clientID = "382277298389-1238ghqiocgfunrk9i1ir9dj2k615cth.apps.googleusercontent.com"
        self.m_google_signIn?.delegate = self
        self.m_google_signIn?.signIn()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //TODO: send this infos to profile view controller
//        if segue.identifier == "id_jobslistview" {
//            let destView = segue.destination as! JobsListViewController
//            NSLog("id : \(self.user_id)")
//            NSLog("id_token : \(self.user_id_token)")
//            NSLog("name : \(self.user_first_name) \(self.user_last_name)")
//            NSLog("email : \(self.user_email)")
//            NSLog("profile : \(self.user_profile_url)")
//        }
        
    }
    
}

extension LoginViewController : GIDSignInDelegate
{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard let user = user else {
            print("Uh oh. The user cancelled the Google login.")
                        return
        }
        
        let userId = user.userID ?? ""
        print("Google User ID: \(userId)")
//        self.user_id = userId
                    
        let userIdToken = user.authentication.idToken ?? ""
        print("Google ID Token: \(userIdToken)")
//        self.user_id_token = userIdToken
                    
        let userFirstName = user.profile.givenName ?? ""
        print("Google User First Name: \(userFirstName)")
//        self.user_first_name = userFirstName
                    
        let userLastName = user.profile.familyName ?? ""
        print("Google User Last Name: \(userLastName)")
//        self.user_last_name = userLastName
                    
        let userEmail = user.profile.email ?? ""
        print("Google User Email: \(userEmail)")
//        self.user_email = userEmail
                    
        let googleProfilePicURL = user.profile.imageURL(withDimension: 150)?.absoluteString ?? ""
        print("Google Profile Avatar URL: \(googleProfilePicURL)")
//        self.user_profile_url = googleProfilePicURL
        
        self.m_user_model = UserModel(userId: userId, userIdToken: userIdToken, user_first_name: userFirstName, user_last_name: userLastName, user_email: userEmail, user_profile_url: googleProfilePicURL)
        
        // Goto Job list view controller
        
        let vc : MainNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "id_main_navigation") as! MainNavigationController
        
        vc.m_user_model = self.m_user_model
        self.present(vc, animated: false, completion: nil)
    }
                
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
                    // Call your backend server to delete user's info after they requested to delete their account
    }
}
