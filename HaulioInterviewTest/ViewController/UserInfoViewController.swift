//
//  UserInfoViewController.swift
//  HaulioInterviewTest
//
//  Created by twizzyindy on 8/5/20.
//  Copyright © 2020 twizzyindy. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "userinfolistcell", for: indexPath)
        
        if indexPath.row == 0 {
            cell.textLabel!.text = "UserName"
            cell.detailTextLabel!.text = "\(self.m_user_model.user_first_name) \(self.m_user_model.user_last_name)"
        }
        
        if indexPath.row == 1 {
            cell.textLabel!.text = "Mail"
            cell.detailTextLabel!.text = "\(self.m_user_model.user_email)"
        }
        return cell
        
    }
    

    @IBOutlet var imgProfilePicture: UIImageView!
    @IBOutlet var tableViewUserInfos: UITableView!
    
    var m_user_model = UserModel(userId: "", userIdToken: "", user_first_name: "", user_last_name: "", user_email: "", user_profile_url: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        imgProfilePicture.layer.borderWidth = 1
        imgProfilePicture.layer.borderColor = UIColor.black.cgColor
        imgProfilePicture.layer.cornerRadius = imgProfilePicture.frame.height/2
        imgProfilePicture.layer.masksToBounds = false
        imgProfilePicture.clipsToBounds = true
        
        imgProfilePicture.load(url: URL(string: self.m_user_model.user_profile_url)!)
        
        // tableview delegate source
        self.tableViewUserInfos.delegate = self
        self.tableViewUserInfos.dataSource = self
    }
    
    func setUserModel(model: UserModel)
    {
        self.m_user_model = model
    }
    
    func getUserModel() -> UserModel
    {
        return self.m_user_model
    }
    
}

//MARK: UIImageView Extension
extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
