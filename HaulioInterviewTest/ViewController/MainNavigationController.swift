//
//  MainNavigationController.swift
//  HaulioInterviewTest
//
//  Created by twizzyindy on 8/4/20.
//  Copyright © 2020 twizzyindy. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {

    var m_user_model = UserModel(userId: "", userIdToken: "", user_first_name: "", user_last_name: "", user_email: "", user_profile_url: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.barTintColor = .red

        // Do any additional setup after loading the view.
    }
    
    func setUserModel(model: UserModel)
    {
        self.m_user_model = model
    }
    
    func getUserModel() -> UserModel
    {
        return self.m_user_model
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
