//
//  AppDelegate.swift
//  HaulioInterviewTest
//
//  Created by twizzyindy on 8/4/20.
//  Copyright © 2020 twizzyindy. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let GMSServicesKeyValid = GMSServices.provideAPIKey("AIzaSyDuijxbl2q6x1XHODNw6BGSlu4hwEyYhkg")
        let GMSServicesSDKVer = GMSServices.sdkVersion()
        print("GMS Services Key Valid: ", GMSServicesKeyValid)
        print("GMS Services SDK version: ", GMSServicesSDKVer)
        
        
        let GMSPlacesKeyValid = GMSPlacesClient.provideAPIKey("AIzaSyACurSx8bslPGxE7OqUWDQoPpU1_U7nmvA")
        let GMSPlacesSDKVer = GMSPlacesClient.sdkVersion()
        print("GMS Places Key Valid: ", GMSPlacesKeyValid)
        print("GMS Places SDK version: ", GMSPlacesSDKVer)

        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return (GIDSignIn.sharedInstance()?.handle(url))!
    }


}

