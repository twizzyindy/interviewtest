//
//  File.swift
//  HaulioInterviewTest
//
//  Created by twizzyindy on 8/4/20.
//  Copyright © 2020 twizzyindy. All rights reserved.
//

import Foundation
import UIKit

struct GeolocationType : Decodable {
    let latitude : Double
    let longitude : Double
}

struct JobStruct : Decodable {
    let id : Int
    let job_id : Int
    let priority : Int
    let company : String
    let address : String
    let geolocation : GeolocationType

    enum CodingKeys: String, CodingKey {
        case geolocation //= "geolocation"
        case id = "id"
        case job_id = "job-id"
        case priority = "priority"
        case company = "company"
        case address = "address"
    }
}

