//
//  File.swift
//  HaulioInterviewTest
//
//  Created by twizzyindy on 8/4/20.
//  Copyright © 2020 twizzyindy. All rights reserved.
//

import Foundation
import UIKit

struct UserModel : Decodable {
    let userId : String
    let userIdToken : String
    let user_first_name : String
    let user_last_name : String
    let user_email : String
    let user_profile_url : String
    
    enum CodingKeys: String, CodingKey{
        case userId
        case userIdToken
        case user_first_name
        case user_last_name
        case user_email
        case user_profile_url
    }
}

