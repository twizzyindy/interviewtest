//
//  JobItemTableViewCell.swift
//  HaulioInterviewTest
//
//  Created by twizzyindy on 8/5/20.
//  Copyright © 2020 twizzyindy. All rights reserved.
//

import UIKit

class JobItemTableViewCell: UITableViewCell {

    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var lblTitle: UILabel!
        
    @IBOutlet var btnAccept: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnAccept.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
